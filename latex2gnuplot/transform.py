"""Transform functions in LaTeX into the syntax of gnuplot"""

import sys
import os
import collections
import operator
from typing import Iterable
import uuid

sys.path.append(os.path.join(os.path.dirname(__file__), '..', 'lark-1.1.5'))
import lark  # noqa
sys.path.pop()


SCALAR_LINE = 'SCALAR: "x"'
VECTOR_LINE = 'VECTOR: "z"'
with open(os.path.join(os.path.dirname(__file__), 'grammar.lark')) as f:
    GRAMMAR = f.read()
assert SCALAR_LINE in GRAMMAR
assert VECTOR_LINE in GRAMMAR


class TransformError(Exception):
    """The formula could not be transformed"""


class FormulaError(TransformError):
    """The formula could not be parsed"""


class ProjectionError(TransformError):
    """A projection failed"""


class ParserCache:
    """Cache parsers for specific variable combinations"""
    def __init__(self, grammar: str):
        self.cache = {}
        self.grammar = grammar

    def get(self, scalars: Iterable[str], vectors: Iterable[str]):
        """Return an appropriate parser for ``scalars`` and ``vectors``"""
        scalars, vectors = map(frozenset, (scalars, vectors))
        try:
            return self.cache[scalars, vectors]
        except KeyError:
            # Why do we add these weird UUID rules?
            # Avoids empty terminals, which are an error
            grammar = (self.grammar
                .replace(SCALAR_LINE, 'SCALAR: ' + ' | '.join(f'"{x}"' for x in (uuid.uuid4(), *scalars)))
                .replace(VECTOR_LINE, 'VECTOR: ' + ' | '.join(f'"{x}"' for x in (uuid.uuid4(), *vectors)))
            )
            self.cache[scalars, vectors] = lark.Lark(grammar, parser='lalr')
            return self.cache[scalars, vectors]


def auto(latex: str, proj_rules: list[str], variables: list[str], scalars: list[str], vectors: list[str]) \
        -> Iterable[tuple[str, str, str]]:
    """Transform the LaTeX formula to gnuplot syntax, guessing variable types

    :param latex: the LaTeX formula
    :param proj_rules: projection rules, may be empty if ``len(scalars) == 1 and not vectors``
        (defaulting to projecting the one variable)
    :param variables: contains variables of unknown type (vector/scalar)
    :param scalars: scalar variables (i.e. without index)
    :param vectors: vector variables (i.e. with index)
    :return: as :func:`with_vars`
    :raises: as :func:`with_vars`, if all guessing attempts fail or a projection fails
    """
    for n in range(1<<len(variables)):
        try:
            yield from with_vars(
                latex,
                proj_rules,
                scalars + [v for i, v in enumerate(variables) if ~n & (1<<i)],
                vectors + [v for i, v in enumerate(variables) if n & (1<<i)],
            )
            return
        except FormulaError:
            continue
    raise TransformError(f'failed to parse {latex!r} with all possible types for {variables}')


def with_vars(latex: str, proj_rules: list[str], scalars: list[str], vectors: list[str]) \
        -> Iterable[tuple[str, str, str]]:
    """Transform the LaTeX formula to gnuplot syntax

    :param latex: the LaTeX formula
    :param proj_rules: projection rules, may be empty if ``len(scalars) == 1 and not vectors``
        (defaulting to projecting the one variable)
    :param scalars: scalar variables (i.e. without index)
    :param vectors: vector variables (i.e. with index)
    :return: a tuple of ``(gnuplot format, projection target, projection rule)`` for each successful projection
    :raises ProjectionError: after producing all possible gnuplot-formats
        if a projection specification is malformed
    :raises FormulaError: before producing any gnuplot-formats
        if ``latex`` cannot be parsed
    """
    if not hasattr(with_vars, 'pc'):
        with_vars.pc = ParserCache(GRAMMAR)
    try:
        tree = with_vars.pc.get(scalars, vectors).parse(latex)
    except lark.LarkError as e:
        raise FormulaError(f'failed to parse {latex!r}: {e}')
    failed_projs = {}
    if not proj_rules:
        if len(scalars) == 1 and not vectors:
            proj_rules = [scalars[0]]
        else:
            raise ProjectionError('no rules specified')
    for rule in proj_rules:
        try:
            vals = Values(rule, scalars, vectors)
            r = vals.unparse(tree)
        except TransformError as e:
            failed_projs[rule] = e
        else:
            target = '_'.join(vals.proj)
            yield r, target, rule.replace(target, '').strip().replace(' ', ', ')
    if failed_projs:
        raise ProjectionError(f'projections failed: {failed_projs}')


class Values:
    """Provide an unparsing function for a specific projection

    :cvar pc: :class:`ParserCache` instance for the projection spec mini-language
    :ivar proj: the projection target, as single-element list for scalars and
        ``[vector, index]`` for vector elements
    :type proj: list[str]
    """
    joinables = {'mult': '*', 'power': '**', 'frac': '/'}
    joinables.update(dict.fromkeys(('sum', 'number', 'neg'), ''))
    func_names = {'Im': 'imag', 'Re': 'real'}
    constants = {'i': '{0,1}'}

    pc = ParserCache(f"""
        start: variable ( " " assn)*
        assn: variable "=" NUMBER
        ?variable: scalar | vector
        scalar: SCALAR
        vector: VECTOR "_" INDEX
        STAR: "*"
        INDEX: INT | STAR
        {SCALAR_LINE} | STAR
        {VECTOR_LINE}
        %import common.INT
        %import common.NUMBER
    """)

    def __init__(self, rule: str, scalars: Iterable[str], vectors: Iterable[str]):
        try:
            proj, *mapped = self.pc.get(scalars, vectors).parse(rule).children
        except lark.LarkError:
            raise TransformError('malformed rule')
        self.proj = list(proj.children)

        self.values = collections.defaultdict(dict)
        defaults = {}
        for (var, val) in map(operator.attrgetter('children'), mapped):
            if var.data == 'scalar':
                name = index = var.children[0]
            else:
                name, index = var.children
            if index in self.values[name] or (name in defaults and index == '*'):
                raise TransformError('may not specify multiple values')
            if index == '*':
                defaults[name] = val
            else:
                self.values[name][index] = val
        for k, v in defaults.items():
            self.values[k] = collections.defaultdict(lambda: v, self.values[k])

    def scalar(self, name):
        if self.proj == [name]:
            return 'x'
        return self.vector(name, name)

    def vector(self, name, index):
        if self.proj == [name, index]:
            return 'x'
        try:
            return self.values[name][index]
        except KeyError:
            try:
                return self.values['*'][None]
            except KeyError:
                raise TransformError('projection rule underspecified')

    def unparse(self, tree: lark.Tree | str) -> str:
        """Unparse ``tree`` to the corresponding gnuplot syntax

        Respect the projection specification use to initialize the class.
        """
        if isinstance(tree, str):
            return tree
        elif tree.data == 'scalar':
            return self.scalar(tree.children[0])
        elif tree.data == 'vector':
            return self.vector(*tree.children)
        elif tree.data in self.joinables:
            op = self.joinables[tree.data]
            return f'({op.join(map(self.unparse, tree.children))})'
        elif tree.data == 'function':
            name, arg = tree.children
            return f'{self.func_names.get(name, name)}({self.unparse(arg)})'
        elif tree.data == 'constant':
            return f'({self.constants[tree.children[0]]})'
        # and some special cases...
        elif tree.data == 'nroot':
            _, n, val = tree.children
            return f'({self.unparse(val)}**(1/{self.unparse(n)}))'
        elif tree.data in ('floor', 'ceil'):
            return f'{tree.data}({self.unparse(tree.children[0])})'
        elif tree.data == 'logn':
            _, n, arg = map(self.unparse, tree.children)
            return f'(log({arg})/log({n}))'
        else:
            raise ValueError(f'invalid node type: "{tree.data}"')
