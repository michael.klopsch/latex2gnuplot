"""
This module includes classes to represent a LaTeX document as
tree of math regions with gnuplot options and extra attributes.
It also includes the parsing logic to create the tree representation
from a document string as well as logic to re-stringify the tree.
"""
from __future__ import annotations
import re
import abc
import logging
from typing import Iterator
import dataclasses

logger = logging.getLogger('document')

# or, more honestly: something we know we can't handle
PROBABLY_NO_FORMULA = ('\\iff', '\\equiv', '\\underbrace', '\\overbrace', '\\implies')

REGION_MARKERS = {'$$': '$$', '%gnuplot begin\n': '%gnuplot end\n'}
REGION_MARKERS.update({
    f'\\begin{{{name}}}': f'\\end{{{name}}}'
    for base_name in ('align', 'aligned', 'equation')
    for name in (base_name, base_name + '*')
})
RE_REGION = re.compile('|'.join(
    f'({re.escape(k)}|{re.escape(v)})' for k, v in REGION_MARKERS.items()))

RE_COMMENT = re.compile('%.*\\n')
RE_OPTION = re.compile('%gnuplot (set .*)\n')
RE_PROJ = re.compile('%gnuplot proj (.*)\n')
RE_ATTR = re.compile('%gnuplot (\\w+)=(.*)\n')
RE_PLOT_OPT = re.compile('%gnuplot plotopt (.*)\n')

RE_FUNC_ARG = re.compile(r'[a-z](_(?:\d|\{\d+\}))?')
RE_FUNC_ARG_VEC1 = re.compile(r'\\[a-zA-Z*]+\s+([a-z])')
RE_FUNC_ARG_VEC2 = re.compile(r'\\[a-zA-Z*]+\s*\{\s*([a-z])\s*\}')
_one_arg = f'(?:{RE_FUNC_ARG.pattern}|{RE_FUNC_ARG_VEC1.pattern}|{RE_FUNC_ARG_VEC2.pattern})'
_opening = r'(?:(?:\\left)?(?:\(?\s*\\begin\{[a-zA-Z*]+\}|\()\s*)'
_closing = r'(?:\s*(?:\\right)?(?:\)?\s*\\end\{[a-zA-Z*]+\}|\)))'
_args = f'({_one_arg}(?:\\s*(?:,|\\\\\\\\)\\s*{_one_arg})*)'
RE_FUNC_SIG_EQ = re.compile(f'[a-zA-Z*]+\\s*{_opening}{_args}{_closing}\\s*=')
RE_FUNC_SIG_MAPS = re.compile(f'{_opening}?{_args}{_closing}?\\s*\\\\mapsto')
del _one_arg, _opening, _closing, _args


class Region(abc.ABC):
    """Abstract math-Mode region."""
    @abc.abstractmethod
    def extract_formulae(self, context: Context) -> Iterator[Formula]:
        """Extract formulae in leaf nodes of the region.

        :param context: meta-data defined in parent regions
        """
        raise NotImplementedError


@dataclasses.dataclass
class Context:
    """Meta-data attached to a formula.

    :ivar options: contains gnuplot option commands present in parent regions.
    :ivar attrs: contains internal attributes defined in parent regions.
    :ivar proj_rules: contains projection rules present in parent regions.
    :ivar plot_opts: (read-only) contains plotting options defined in parent regions
        and duplicate-filterd to avoid double options
    """
    options: list[str] = dataclasses.field(default_factory=list)
    attrs: dict[str, str] = dataclasses.field(default_factory=dict)
    proj_rules: list[str] = dataclasses.field(default_factory=list)
    _plot_opts: dict[str, str] = dataclasses.field(default_factory=dict)

    @property
    def plot_opts(self) -> set[str]:
        return set(self._plot_opts.values())

    def update(self, text: str):
        """update in-place with meta-data in ``text``"""
        self.options += RE_OPTION.findall(text)
        self.attrs |= RE_ATTR.findall(text)
        self.proj_rules += RE_PROJ.findall(text)
        self._plot_opts |= {o.split()[0]: o for o in RE_PLOT_OPT.findall(text)}

    def copy(self) -> Context:
        """Perform a deep copy"""
        return Context(*dataclasses.astuple(self))


@dataclasses.dataclass
class Leaf(Region):
    """Math-mode region with no subregions, potential formula"""
    opening: str
    content: str
    closing: str
    plot: str = ''

    def extract_formulae(self, context: Context) -> Iterator[Formula]:
        """Implement the abstract method in :class:`Region`.

        See :meth:`Region.extract_formulae` for details.
        This generator will yield at most one value.
        """
        content = RE_COMMENT.sub('', self.content.strip())
        if not content or any(x in content for x in PROBABLY_NO_FORMULA):
            return
        context.update(self.content)
        args = [], [], []
        if m := RE_FUNC_SIG_EQ.match(content) or RE_FUNC_SIG_MAPS.match(content):
            content = content[m.end():]
            for arg in map(str.strip, m.group(1).replace('\\\\', ',').split(',')):
                if (am := RE_FUNC_ARG_VEC1.match(arg) or RE_FUNC_ARG_VEC2.match(arg)):
                    args[2].append(am.group(1))
                else:
                    args['_' in arg].append(arg)
        else:
            if not context.proj_rules:
                context.proj_rules = ['x']
            for rule in context.proj_rules:
                for i, x in enumerate(rule.split()):
                    if x[0] != '*':
                        args[2*('_' in x) or not i].append(x[0])

        yield Formula(content, *args, context, self)

    def __str__(self):
        return ''.join((self.opening, self.content, self.closing))


@dataclasses.dataclass
class Node(Region):
    """Math-Mode region with subregions"""
    text: list[str]
    regions: list[Region]

    def __port_init(self):
        assert len(self.text) == len(self.regions) + 1, (self.text, self.regions)

    @property
    def plot(self):
        return ''.join(r.plot for r in self.regions)

    def extract_formulae(self, context: Context) -> Iterator[Formula]:
        for t, r in zip(self.text, self.regions):
            context.update(t)
            yield from r.extract_formulae(context.copy())

    def __str__(self):
        return ''.join(
            str((self.text, self.regions)[i%2][i//2])
            for i in range(2*len(self.text) - 1)
        )


@dataclasses.dataclass
class Formula:
    """A single formula, with variable hints and settings.

    :ivar latex: the formula itself, in LaTeX syntax
    :ivar variables: formula variables that may be scalars or vectors (e.g. ``x``)
    :ivar scalars: scalar formula variables (e.g. ``x_0``)
    :ivar vectors: vector formula variables (e.g. ``\\bar x``)
    :ivar context: metadata, see :class:`Context` for details
    :ivar plot: (write-only) set the LaTeX code for plotting this formula

    If all of :attr:`variables`, :attr:`scalars` and :attr:`vectors`
    are empty, no variable list could be found.
    """
    latex: str
    variables: list[str]
    scalars: list[str]
    vectors: list[str]
    context: Context
    _leaf: Leaf

    @(lambda f: property(None, f, None, f.__doc__))
    def plot(self, value):
        self._leaf.plot += value


class Document:
    """Complete LaTeX document, root of the region tree"""
    node: Node

    def __init__(self, data: str):
        def make_region():
            current.pop()
            if (rs := regions.pop()):
                s1, s2, *inner, e1, e2 = text.pop()
                return Node([s1+s2, *inner, e1+e2], rs)
            else:
                return Leaf(*text.pop())

        def recover(error):
            nonlocal text, regions
            logger.error(f'failed to build math-mode tree: {error}')
            trail = []
            for text_, regions_ in zip(text[1:], regions[1:]):
                if regions_:
                    s1, s2, *inner, e1, e2 = text_
                    text_ = (s1+s2, *inner, e1+e2)
                    assert len(text_) == len(regions_) + 1
                    trail.extend((text_, regions_)[i%2][i//2] for i in range(2*len(text_) - 1))
                else:
                    trail.extend(text_)
            trail.append(data[last_idx:])
            text[0].append(''.join(map(str, trail)))
            text = [text[0]]
            regions = [regions[0]]
            self.node = make_region()

        text = [['']]
        regions = [[]]
        current = [None]
        last_idx = 0
        for m in RE_REGION.finditer(data):
            text[-1].append(data[last_idx:m.start()])
            last_idx = m.end()
            if current[-1] == m.group():  # closing
                text[-1].append(m.group())
                regions[-2].append(make_region())
            elif (closer := REGION_MARKERS.get(m.group())):  # opening
                current.append(closer)
                text.append([m.group()])
                regions.append([])
            else:  # not good
                text[-1].append(m.group())
                return recover(f'got unexpected closing {m.group()}')
        text[-1].append('')
        if len(current) != 1:
            text[-1].append('')  # the trailing data is added in recover
            return recover(f'expected {current[-1]}, but got EOF')
        text[-1].append(data[last_idx:])
        self.node = make_region()
        logger.debug(f'parsed document: {self.node!r}')

    def extract_formulae(self) -> Iterator[Formula]:
        """extract the formulae in the document"""
        return self.node.extract_formulae(Context(_plot_opts={'title': 'title ""'}))

    def __str__(self):
        return ''.join(
            str(t) + str(r) + r.plot for t, r in zip(self.node.text, self.node.regions)
        ) + str(self.node.text[-1])
