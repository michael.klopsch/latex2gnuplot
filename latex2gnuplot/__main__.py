#!/usr/bin/env python3
"""LaTeX2gnuplot  -- plot formulae in LaTeX documents."""

import argparse
import sys
import logging
import string
import asyncio
from . import document
from . import gnuplot
from . import transform


DEFAULT_FIGURE_TMPLS = (
    r'\begin{figure} \centering &plot \caption{ $ &latex $ } \end{figure}',
    r'\begin{figure} \centering &plot \caption{ $ &latex $ projected on $ &target $ with $ &rule$ } \end{figure}',
)


class AmpTemplate(string.Template):
    delimiter = '&'


def parse_args():
    parser = argparse.ArgumentParser()
    parser.add_argument('--gnuplot', default='gnuplot', help='gnuplot executable')
    parser.add_argument('--create-style', action='store_true', help='create required TikZ style files')
    parser.add_argument('--debug-parse', help='display the parse tree of the given formula')
    parser.add_argument('--extra-procs', type=int, default=0, help='use more gnuplot proceses')
    parser.add_argument('-v', '--verbose', action='count', default=0, help='more output')
    parser.add_argument('-q', '--quiet', action='count', default=0, help='less output')
    return parser.parse_args()


async def set_plots(pm: gnuplot.ProcessManager, f: document.Formula):
    plots = []
    multi = False
    try:
        for gp, target, rule in transform.auto(f.latex, f.context.proj_rules, f.variables, f.scalars, f.vectors):
            try:
                plot = await pm.submit(f.context.options + [f'plot {gp} {" ".join(f.context.plot_opts)}', 'reset'])
            except gnuplot.ProcessError as e:
                logging.error(f'failed to plot {gp!r} (latex {f.latex!r}): {e}')
                continue
            plots.append({'plot': plot, 'target': target, 'rule': rule})
            multi = bool(rule)  # if we don't specify anything, we're one-dimensional
    except transform.TransformError as e:
        logging.error(f'error transforming {f.latex!r}: {e}')
        logging.debug(f'detailed formula data: {f!r}')
    tmpl = AmpTemplate(f.context.attrs.get('figure_tmpl', DEFAULT_FIGURE_TMPLS[multi]))
    try:
        f.plot = ''.join(tmpl.substitute(f.context.attrs, latex=f.latex, **p) for p in plots)
    except KeyError as e:
        logging.error(f'failed to format {f.latex!r}: {e!r}')
        logging.debug(f'detailed formula data: {f!r}')


async def gather(coros):
    await asyncio.gather(*coros)


def main(args):
    logging.basicConfig(
        level=10*(3 - args.verbose + args.quiet),
    )
    if args.debug_parse:
        tree = transform.lark.Lark(transform.GRAMMAR, parser='lalr').parse(args.debug_parse)
        print(tree.pretty())
        print(transform.Values('x', ('x',), ()).unparse(tree))
        sys.exit()
    data = sys.stdin.read()
    doc = document.Document(data)
    pm = gnuplot.ProcessManager(args.gnuplot, args.extra_procs)
    coros = []
    if args.create_style:
        coros.append(pm.submit(['set term tikz createstyle']))
    for f in doc.extract_formulae():
        coros.append(set_plots(pm, f))
    asyncio.run(gather(coros))
    sys.stdout.write(str(doc))


if __name__ == '__main__':
    main(parse_args())
