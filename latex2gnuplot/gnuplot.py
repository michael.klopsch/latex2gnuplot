import asyncio
import asyncio.subprocess
import functools
import logging
import contextlib
import uuid

# determined unscientifically by testing on my laptop
PLOT_PROCESS_RATIO = 1000

logger = logging.getLogger('gnuplot')


class ProcessError(Exception):
    """gnuplot wrote something to stderr"""


class Process:
    """Wrapper around ``asyncio.subprocess.Process``"""
    def __init__(self, proc: asyncio.subprocess.Process):
        self.proc = proc
        self.error_task = asyncio.Task(self._wait_error())

    def __getattr__(self, name):
        return getattr(self.proc, name)

    async def _wait_error(self):
        async for line in self.proc.stderr:
            line = line.decode().strip()
            if line.startswith('Warning'):
                logger.warning(f'gnuplot warning: {line}')
            elif line:
                raise ProcessError(line)

    def or_error(coro):
        @functools.wraps(coro)
        async def wrapper(self, *args, **kwargs):
            task = asyncio.Task(coro(self, *args, **kwargs))
            done, pending = await asyncio.wait(
                (self.error_task, task),
                return_when=asyncio.FIRST_COMPLETED,
            )
            r = None
            for d in done:
                if d.exception():
                    raise d.exception()
                r = d.result()
            return r

        return wrapper

    @or_error
    async def write_lines(self, lines: list[str]):
        """write ``lines``, adding ``\\n`` and await drain()"""
        assert isinstance(lines, list), lines
        self.proc.stdin.writelines([line.encode() + b'\n' for line in lines])
        await self.proc.stdin.drain()

    @or_error
    async def read_until(self, target: str) -> str:
        """read lines up to and not including one containing ``target``, which is discarded"""
        lines = []
        async for line in self.proc.stdout:
            if target == line.decode():
                logger.debug(f'got {line}, returning {len(lines)} lines')
                return ''.join(lines)
            lines.append(line.decode())

    del or_error


class ProcessManager:
    """Process Manager to plot the functions"""
    def __init__(self, exe: str, extra_procs: int):
        self.exe = exe
        self.queue = asyncio.Queue()
        self.nprocs = -extra_procs
        self.nwaiting = 0

    @contextlib.asynccontextmanager
    async def get_process(self):
        """Context manager for a :class:`Process` instance"""
        if self.nprocs * PLOT_PROCESS_RATIO <= self.nwaiting:
            self.nprocs += 1
            proc = Process(await asyncio.create_subprocess_exec(
                self.exe,
                stdin=asyncio.subprocess.PIPE,
                stdout=asyncio.subprocess.PIPE,
                stderr=asyncio.subprocess.PIPE,
            ))
            logger.info(f'started new gnuplot process with pid {proc.pid}')
            await proc.write_lines(['set print "-"', 'set term tikz'])
        else:
            self.nwaiting += 1
            proc = await self.queue.get()
            self.nwaiting -= 1
        try:
            yield proc
        except Exception as e:
            self.nprocs -= 1
            if proc.returncode is None:
                proc.terminate()
            async with self.get_process() as sproc:
                pass  # create a substitute process, inserted after exiting the manager
            raise
        else:
            await self.queue.put(proc)

    async def submit(self, cmds: list[str]) -> str:
        """run ``cmds`` and return the output

        :param cmds: are the commands to run

        The implementation uses the ``print`` command internally,
        so its setting should not be interfered with.
        """
        end_marker = uuid.uuid4()
        async with self.get_process() as proc:
            await proc.write_lines(cmds + [f'print "{end_marker}"'])
            return await proc.read_until(f'{end_marker}\n')
