import pytest
from latex2gnuplot.transform import lark, GRAMMAR

parser = lark.Lark(GRAMMAR, parser="lalr")



def test_simple():
    assert parser.parse('2x^2').pretty() == 'mult\n  number\t2\n  power\n    scalar\tx\n    number\t2\n'

    assert parser.parse('-3x').pretty() == 'mult\n  neg\n    -\n    number\t3\n  scalar\tx\n'

    assert parser.parse('2\\sin 2x + 7').pretty() == (
        'sum\n'
        '  mult\n'
        '    number\t2\n'
        '    function\n'
        '      sin\n'
        '      mult\n'
        '        number\t2\n'
        '        scalar\tx\n'
        '  +\n'
        '  number\t7\n')


def test_sqrt():
    assert parser.parse('\\sqrt 5').pretty() == 'function\n  sqrt\n  number\t5\n'

    assert parser.parse('2\\sqrt2').pretty() == 'mult\n  number\t2\n  function\n    sqrt\n    number\t2\n'

    assert parser.parse('\\sqrt{5}').pretty() == 'function\n  sqrt\n  number\t5\n'

    assert parser.parse('\\sqrt[\\sin x] 2').pretty() == (
        'nroot\n'
        '  sqrt\n'
        '  function\n'
        '    sin\n'
        '    mult\n'
        '      scalar\tx\n'
        '  number\t2\n')


def test_nroot():
    assert parser.parse('\\sqrt[2] 4').pretty() == 'nroot\n  sqrt\n  number\t2\n  number\t4\n'

    assert parser.parse('\\sqrt [ 4 ] 1').pretty() == 'nroot\n  sqrt\n  number\t4\n  number\t1\n'


def test_mult_nums():
    assert parser.parse('2xxx').pretty() == 'mult\n  number\t2\n  scalar\tx\n  scalar\tx\n  scalar\tx\n'

    assert parser.parse('2x^2x^x').pretty() == (
        'mult\n'
        '  number\t2\n'
        '  power\n'
        '    scalar\tx\n'
        '    number\t2\n'
        '  power\n'
        '    scalar\tx\n'
        '    scalar\tx\n')

    assert parser.parse('2').pretty() == 'number\t2\n'

    with pytest.raises(lark.UnexpectedToken):
        parser.parse('x2')

    with pytest.raises(lark.UnexpectedToken):
        parser.parse('2 2^x')

    assert parser.parse('22^x').pretty() == 'power\n  number\t22\n  scalar\tx\n'

    assert parser.parse('x 2^x').pretty() == 'mult\n  scalar\tx\n  power\n    number\t2\n    scalar\tx\n'

    assert parser.parse('2x2^x').pretty() == (
        'mult\n'
        '  number\t2\n'
        '  scalar\tx\n'
        '  power\n'
        '    number\t2\n'
        '    scalar\tx\n')


def test_mult_end():
    assert parser.parse('2x\\sin x').pretty() == (
        'mult\n'
        '  number\t2\n'
        '  scalar\tx\n'
        '  function\n'
        '    sin\n'
        '    mult\n'
        '      scalar\tx\n')

    assert parser.parse('2\\sin(x)x').pretty() == (
        'mult\n'
        '  number\t2\n'
        '  function\n'
        '    sin\n'
        '    scalar\tx\n'
        '  scalar\tx\n')

    assert parser.parse('2 \\sin (  5 ) 2^x').pretty() == (
        'mult\n'
        '  number\t2\n'
        '  function\n'
        '    sin\n'
        '    number\t5\n'
        '  power\n'
        '    number\t2\n'
        '    scalar\tx\n')

    with pytest.raises(lark.UnexpectedToken):
        parser.parse('2\\sin x \\cdot 8')

    assert parser.parse('2 \\cdot 7').pretty() == 'mult\n  number\t2\n  number\t7\n'


def test_negs():
    assert parser.parse('-3').pretty() == 'neg\n  -\n  number\t3\n'

    assert parser.parse('-3x').pretty() == 'mult\n  neg\n    -\n    number\t3\n  scalar\tx\n'

    assert parser.parse('-3\\sin x').pretty() == (
        'mult\n'
        '  neg\n'
        '    -\n'
        '    number\t3\n'
        '  function\n'
        '    sin\n'
        '    mult\n'
        '      scalar\tx\n')

    assert parser.parse('+2++2').pretty() == (
        'sum\n'
        '  neg\n'
        '    +\n'
        '    number\t2\n'
        '  +\n'
        '  neg\n'
        '    +\n'
        '    number\t2\n')

    with pytest.raises(lark.UnexpectedToken):
        parser.parse('---1')


def test_numbers():
    assert parser.parse('1').pretty() == 'number\t1\n'

    assert parser.parse('1.5').pretty() == 'number\t1.5\n'

    assert parser.parse('.3').pretty() == 'number\t.3\n'

    with pytest.raises(lark.UnexpectedCharacters):
        parser.parse('1,0')


def test_texatoms():
    assert parser.parse('\\frac23').pretty() == 'frac\n  number\t2\n  number\t3\n'

    assert parser.parse('\\frac{1}{2}').pretty() == 'frac\n  number\t1\n  number\t2\n'

    assert parser.parse('\\frac{1}2').pretty() == 'frac\n  number\t1\n  number\t2\n'

    assert parser.parse('\\frac 5{ 3+6 }').pretty() == (
        'frac\n'
        '  number\t5\n'
        '  sum\n'
        '    number\t3\n'
        '    +\n'
        '    number\t6\n')


def test_floorceil():
    assert parser.parse('\\lfloor 2+3 \\rfloor').pretty() == 'floor\n  sum\n    number\t2\n    +\n    number\t3\n'

    assert parser.parse('\\left\\lceil 2^x \\right\\rceil').pretty() == 'ceil\n  power\n    number\t2\n    scalar\tx\n'


def test_log():
    assert parser.parse('\\log 5').pretty() == 'function\n  log\n  mult\n    number\t5\n'

    assert parser.parse('2\\log(5)x').pretty() == (
        'mult\n'
        '  number\t2\n'
        '  function\n'
        '    log\n'
        '    number\t5\n'
        '  scalar\tx\n')

    assert parser.parse('\\log_2 7').pretty() == 'logn\n  log\n  number\t2\n  mult\n    number\t7\n'

    assert parser.parse('\\log_{3x^2} 34').pretty() == (
        'logn\n'
        '  log\n'
        '  mult\n'
        '    number\t3\n'
        '    power\n'
        '      scalar\tx\n'
        '      number\t2\n'
        '  mult\n'
        '    number\t34\n')

    assert parser.parse('\\log_23').pretty() == 'logn\n  log\n  number\t2\n  mult\n    number\t3\n'


def test_constants():
    assert parser.parse('2i').pretty() == 'mult\n  number\t2\n  constant\ti\n'

    assert parser.parse('3i+5').pretty() == (
        'sum\n'
        '  mult\n'
        '    number\t3\n'
        '    constant\ti\n'
        '  +\n'
        '  number\t5\n')
