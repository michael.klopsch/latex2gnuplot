import asyncio
import asyncio.subprocess
import pytest

from latex2gnuplot import gnuplot


async def get_proc(cmd):
    return gnuplot.Process(await asyncio.create_subprocess_shell(
        cmd, stdin=asyncio.subprocess.PIPE, stdout=asyncio.subprocess.PIPE, stderr=asyncio.subprocess.PIPE))


@pytest.mark.asyncio
async def test_process_read_write():
    proc = await get_proc('cat')
    await proc.write_lines(['one', 'two', 'three', 'four'])
    assert await proc.read_until('three\n') == 'one\ntwo\n'
    assert await proc.read_until('four\n') == ''


@pytest.mark.asyncio
async def test_process_error():
    proc = await get_proc('{ sleep 0.05; echo err >&2; } & cat')
    await proc.write_lines(['one', 'two', 'three'])
    assert await proc.read_until('two\n') == 'one\n'
    with pytest.raises(gnuplot.ProcessError):
        await proc.read_until('whatever')


class DummyProcess:
    count = 0
    returncode = None
    pid = None

    def __init__(self, _):
        type(self).count += 1
        self.written = []

    def terminate(self):
        self.returncode = 1
        type(self).count -= 1

    async def write_lines(self, lines):
        try:
            await asyncio.sleep(float(lines[0]))
        except ValueError:
            pass
        if 'exit' in lines:
            self.terminate()
            raise gnuplot.ProcessError
        if 'fail' in lines:
            raise gnuplot.ProcessError
        self.written.extend(lines)

    async def read_until(self, _):
        return 'blah\n'

@pytest.mark.asyncio
async def test_process_manager(monkeypatch):
    monkeypatch.setattr(gnuplot, 'PLOT_PROCESS_RATIO', 3)
    monkeypatch.setattr(gnuplot, 'Process', DummyProcess)
    pm = gnuplot.ProcessManager('true', 1)
    assert await pm.submit(['0']) == 'blah\n'
    assert DummyProcess.count == 1
    coros = [pm.submit([str(0.005 + i/500)]) for i in range(10)]
    await asyncio.gather(*coros)
    assert DummyProcess.count == 4 == pm.nprocs + 1
    with pytest.raises(gnuplot.ProcessError):
        await pm.submit(['fail'])
    with pytest.raises(gnuplot.ProcessError):
        await pm.submit(['exit'])
    assert DummyProcess.count == 2 == pm.nprocs + 1
    coros = [pm.submit([str(0.005 + i/500)] + ['fail']*(i%3 == 0)) for i in range(10)]
    await asyncio.gather(*coros, return_exceptions=True)
    assert DummyProcess.count == 2 == pm.nprocs + 1
