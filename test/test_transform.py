import pytest
import collections
from latex2gnuplot import transform

def test_values_grammar():
    parser = transform.Values.pc.get(('x', 'y', 'z'), ('a', 'b', 'c'))

    assert parser.parse('x').pretty() == 'start\n  scalar\tx\n'

    assert parser.parse('x x=1 y=2 z=3').pretty() == (
        'start\n'
        '  scalar\tx\n'
        '  assn\n'
        '    scalar\tx\n'
        '    1\n'
        '  assn\n'
        '    scalar\ty\n'
        '    2\n'
        '  assn\n'
        '    scalar\tz\n'
        '    3\n')

    assert parser.parse('a_23 x=5 b_*=7 x=12 *=2').pretty() == (
        'start\n'
        '  vector\n'
        '    a\n'
        '    23\n'
        '  assn\n'
        '    scalar\tx\n'
        '    5\n'
        '  assn\n'
        '    vector\n'
        '      b\n'
        '      *\n'
        '    7\n'
        '  assn\n'
        '    scalar\tx\n'
        '    12\n'
        '  assn\n'
        '    scalar\t*\n'
        '    2\n')

    with pytest.raises(transform.lark.UnexpectedToken):
        parser.parse('x_1')


class ValuesTest(transform.Values):
    def __init__(self, proj, values):
        self.proj = proj
        self.values = values


def test_values_resolve():
    values = ValuesTest(['n'], {
        '*': collections.defaultdict(lambda: 0),
        'x': {'x': 1},
        'q': {'1': 2, '2': 3},
        'w': {},
    })
    assert values.scalar('n') == 'x'
    assert values.scalar('x') == 1
    assert values.scalar('z') == 0
    assert values.scalar('w') == 0
    assert values.vector('w', '?') == 0
    assert values.vector('q', '1') == 2
    assert values.vector('q', '?') == 0
    assert values.vector('n', '3') == 0

    assert ValuesTest(['x', '5'], {}).vector('x', '5') == 'x'


def test_values_init():
    values = transform.Values('x x=2 y=5 z_*=1 z_4=3', ('x', 'y'), ('z',))
    assert values.proj == ['x']
    vals = dict(values.values)
    assert vals['x'] == {'x': '2'}
    assert vals['y'] == {'y': '5'}
    assert vals['z'].default_factory() == '1'
    assert vals['z'] == {'4': '3'}

    values = transform.Values('z_3 x=5 *=7 z_2=8', ('x', 'y'), ('z',))
    assert values.proj == ['z', '3']
    vals = dict(values.values)
    assert vals['*'].default_factory() == '7'
    assert vals['x'] == {'x': '5'}
    assert vals['z'] == {'2': '8'}

    with pytest.raises(transform.TransformError):
        transform.Values('x x=7 x=8', ('x',), ())

    with pytest.raises(transform.TransformError):
        transform.Values('x *=7 *=8', ('x',), ())

    with pytest.raises(transform.TransformError):
        transform.Values('x_0 x_*=7 x_*=8', (), ('x',))

    with pytest.raises(transform.TransformError):
        transform.Values('x_0 x_5=7 x_5=8', (), ('x',))

    with pytest.raises(transform.TransformError):
        transform.Values('x_0 x=7 x_*=8', (), ('x',))

    transform.Values('x', ['x'], ())
    transform.Values('x_0', (), ['x'])

def test_parser_cache():
    # this verifies we cactually cache and are order insensitive
    pc = transform.ParserCache('start: "x"')
    assert pc.get(['x', 'y'], {'s': 3}) is pc.get('yx', {'s'})


def test_with_vars():
    x = 31.415

    def check(expected, *args):
        # eval is evil, but we don't want to change the tests
        # each time unparse() changes where it puts parens
        assert list(map(lambda e: (eval(e[0], {'x': x}), e[1]), transform.with_vars(*args))) == expected

    check([(x**2, 'x')], 'x^2', ['x'], ['x'], [])
    check([(x**2, 'x_0'), (x, 'x_1')], 'x_0^2+x_1', ['x_0 x_1=0', 'x_1 x_0=0'], [], ['x'])
    check([(x**2, 'x_0'), (x, 'x_1')], 'x_0^2+x_1', ['x_0 x_1=0', 'x_1 x_0=0'], ['x_0', 'x_1'], [])
    check([(x, 'w')], 'w', [], ['w'], [])

    with pytest.raises(transform.ProjectionError):
        list(transform.with_vars('x^2', ['y'], ['x'], []))

    with pytest.raises(transform.FormulaError):
        list(transform.with_vars('x_1^2', ['x_1'], ['x'], []))

    with pytest.raises(transform.ProjectionError):
        list(transform.with_vars('x+y', ['x'], ['x', 'y'], []))


def test_auto():
    x = 31.415

    def check(expected, *args):
        # eval is evil, but we don't want to change the tests
        # each time unparse() changes where it puts parens
        assert list(map(lambda e: eval(e[0], {'x': x}), transform.auto(*args))) == expected

    check([x**2], 'x^2', ['x'], ['x'], [], [])
    check([x**2, x], 'x_0^2+x_1', ['x_0 x_1=0', 'x_1 x_0=0'], ['x'], [], [])
    check([x**2, x], 'x_0^2+x_1', ['x_0 x_1=0', 'x_1 x_0=0'], ['x'], [], [])
    check([x], 'w', [], ['w'], [], [])

    with pytest.raises(transform.ProjectionError):
        list(transform.auto('x^2', ['y'], ['x'], [], []))

    with pytest.raises(transform.TransformError):
        list(transform.auto('x_1^2', ['x_1'], [], ['x'], []))
