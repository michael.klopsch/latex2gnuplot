import pytest
from latex2gnuplot import document

def test_document_init():
    doc = document.Document(r"""
    blah
    \begin{align} simple formula \end{align}
    $$ more \begin{aligned} complex \end{aligned} formula $$
    hem
    $$ simple again $$
    \begin{align} these $$ are $$ two $$ math $$ modes \end{align}
    """)
    assert doc.node == document.Node(
        ['\n    blah\n    ', '\n    ', '\n    hem\n    ', '\n    ', '\n    '],
        [
            document.Leaf('\\begin{align}', ' simple formula ', '\\end{align}'),
            document.Node(
                ['$$ more ', ' formula $$'],
                [document.Leaf('\\begin{aligned}', ' complex ', '\\end{aligned}')],
            ),
            document.Leaf('$$', ' simple again ', '$$'),
            document.Node(
                ['\\begin{align} these ', ' two ', ' modes \\end{align}'],
                [document.Leaf('$$', ' are ', '$$'), document.Leaf('$$', ' math ', '$$')],
            ),
        ],
    )

    doc = document.Document(r"""
    blah
    $$ this is ok $$
    \begin{align} this is wrong \end{aligned}
    $$ what about this? $$
    """)
    assert doc.node == document.Node(
        ['\n    blah\n    ', '\n    \\begin{align} this is wrong \\end{aligned}\n    $$ what about this? $$\n    '],
        [document.Leaf('$$', ' this is ok ', '$$')]
    )

    doc = document.Document(r"""
    blah $$ ok $$ \begin{align} in \begin{aligned} a \end{aligned} problem \end{aligned}
    """)
    assert doc.node == document.Node(
        ['\n    blah ', r' \begin{align} in \begin{aligned} a \end{aligned} problem \end{aligned}' + '\n    '],
        [document.Leaf('$$', ' ok ', '$$')]
    )

    doc = document.Document(r"""
    blah
    $$ this is ok $$
    \begin{align} I'm never closed
    $$ what about this? $$
    """)
    assert doc.node == document.Node(
        ['\n    blah\n    ', "\n    \\begin{align} I'm never closed\n    $$ what about this? $$\n    "],
        [document.Leaf('$$', ' this is ok ', '$$')]
    )

def test_str():
    node = document.Node(
        ['a', 'b', 'c'],
        [document.Leaf('@', 'q', '#'), document.Leaf('$', 'r', '%')]
    )
    doc = document.Document('')
    doc.node = node
    assert str(node) == str(doc) == 'a@q#b$r%c'

    node.regions[0].plot = 'P'
    assert str(node) == 'a@q#b$r%c'
    assert str(doc) == 'a@q#Pb$r%c'

def test_re_func_arg():
    for x in ('x', 'x_1', 'x_{1}', 'x_{ 2 }', 'x_{23}'):
        assert document.RE_FUNC_ARG.match(x)
    for x in ('\\bar x', '\\bar{x}', '\\bar {x}'):
        assert document.RE_FUNC_ARG_VEC1.match(x) or document.RE_FUNC_ARG_VEC2.match(x)

def test_re_func_sig():
    cases = (
        ('f(x) = ', 'x'),
        ('h(x_1, \\bar y )=', 'x_1, \\bar y'),
        (r'g\begin{pm} a \\ b \end{pm} =', r'a \\ b'),
        ('x \\mapsto', 'x'),
        ('x, y \\mapsto', 'x, y'),
        (r'\left( x_0, x_7 \\ q ) \mapsto', 'x_0, x_7 \\\\ q'),
    )
    for val, args in cases:
        m = document.RE_FUNC_SIG_EQ.match(val) or document.RE_FUNC_SIG_MAPS.match(val)
        assert m
        assert m.group(1).strip() == args

def test_leaf_extract_formulae():
    def extract(content):
        return next(document.Leaf('$$', content, '$$').extract_formulae(document.Context()))

    with pytest.raises(StopIteration):
        extract('x+2 = 5 \\implies x = 3')

    f = extract('2y^2')
    assert not f.variables
    assert f.scalars == ["x"]
    assert not f.vectors

    f = extract('f(x, y_0) = 2x')
    assert f.variables == ['x']
    assert f.scalars == ['y_0']
    assert not f.vectors

    f = extract('(\\bar {x}, y) \\mapsto xy')
    assert f.variables == ['y']
    assert not f.scalars
    assert f.vectors == ['x']


def test_leaf_extract_formulae_proj_rule_infer():
    def extract(rules):
        return next(document.Leaf('$$', 'x^2', '$$').extract_formulae(document.Context(proj_rules=rules)))

    f = extract([])
    assert f.scalars == ["x"]
    assert not f.vectors
    assert not f.variables

    f = extract(["z v_2=3 q_*=5", 'x_5 *=8 e=2'])
    assert f.variables == ["e"]
    assert set(f.vectors) == set('qvx')
    assert f.scalars == ["z"]


def test_leaf_extract_extra():
    def extract(content):
        return next(document.Leaf('$$', content, '$$').extract_formulae(
            document.Context(['1'], {'2': '2', 'r': '7'}, [], {'asdf': 'asdf fdsa', 'wad': 'wad'}))).context

    c = extract(
        '%gnuplot set title blah\n'
        '%gnuplot set 42\n'
        '%whatever\n'
        '%gnuplot plotopt blah blim\n'
        '%gnuplot r=5\n'
        '%gnuplot plotopt blah blam\n'
        '%gnuplot plotopt asdf qwert\n'
        '%gnuplot proj blah\n'
        '2x')
    assert c.options == ['1', 'set title blah', 'set 42']
    assert c.attrs == {'2': '2', 'r': '5'}
    assert c.proj_rules == ['blah']
    assert c.plot_opts == {'asdf qwert', 'wad', 'blah blam'}


class DummyLeaf:
    def extract_formulae(self, ctx):
        ctx.update('%gnuplot set 42\n%gnuplot q=3\n%gnuplot proj z\n%gnuplot plotopt optplot\n')
        self.ctx = ctx
        yield 123


def test_node_extract_formulae():
    node = document.Node(
        [
            '%gnuplot set first\n%gnuplot x=5\n',
            '%gnuplot set second\n%gnuplot y=4\n%gnuplot proj blah\n',
            '%gnuplot set third\n%gnuplot z=3\n',
        ],
        [DummyLeaf(), DummyLeaf()],
    )
    assert list(node.extract_formulae(document.Context([1], {'y': 7}, [5]))) == [123, 123]
    assert node.regions[0].ctx == document.Context(
        [1, 'set first', 'set 42'],
        {'x': '5', 'y': 7, 'q': '3'},
        [5, 'z'],
        {'optplot': 'optplot'},
    )
    assert node.regions[1].ctx == document.Context(
        [1, 'set first', 'set second', 'set 42'],
        {'x': '5', 'y': '4', 'q': '3'},
        [5, 'blah', 'z'],
        {'optplot': 'optplot'},
    )


def test_document_extract_formulae():
    doc = document.Document('')
    doc.node = DummyLeaf()
    assert list(doc.extract_formulae()) == [123]
    assert doc.node.ctx == document.Context(['set 42'], {'q': '3'}, ['z'], {'title': 'title ""', 'optplot': 'optplot'})


def test_formula_set_plot():
    leaf = document.Leaf('', '', '')
    f = document.Formula('', [], [], [], None, leaf)
    f.plot = '123'
    assert leaf.plot == '123'

    with pytest.raises(AttributeError):
        f.plot
