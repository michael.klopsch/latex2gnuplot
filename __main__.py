"""Zipapp entry point"""

import runpy
import builtins
import pathlib
import io
import zipfile


zipapp_path = pathlib.Path(__file__).parent
zipapp = zipfile.ZipFile(zipapp_path)

def monkeypatch_open(file, *args, **kwargs):
    file = pathlib.Path(file)
    if file.is_relative_to(zipapp_path):
        return io.TextIOWrapper(zipapp.open(str(file.relative_to(zipapp_path))), *args, **kwargs)
    else:
        return real_open(file, *args, **kwargs)

real_open = builtins.open
builtins.open = monkeypatch_open

runpy.run_module('latex2gnuplot', run_name='__main__')
