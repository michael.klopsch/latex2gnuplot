.. _extending:

Extending LaTeX2gnuplot
=======================

This page describes how to add small enhancements to LaTeX2gnuplot,
tweaking accepted input and behaviour to fit your needs.

.. warning::

  This will require editing the source code and/or grammar directly,
  so it is aimed at users with advanced knowledge and at developers.
  You are trusted to be able to find and edit the files.

Adapting default figure templates
---------------------------------
The default figure templates are defined in ``__main__.py`` right at the beginning.
The first one is for plots without projection, the second one for plots with.


Adapting math regions
---------------------
The region markers are stored in ``REGION_MARKERS`` in ``document.py`` (fairly near the top)
as start marker mapping to end marker.
If adding a ``\begin``-``\end`` environment, you can also just add the name
to the ``('align', 'aligned', 'equation')`` tuple a few lines below.

Adapting function signatures
----------------------------
Function signatures are defined in a group of regexes near the top of ``document.py``.

- ``RE_FUNC_ARG_VEC?`` (1 and 2) are used for checking whether a single argument should be inferred as a vector.
- ``_one_arg`` matches a single argument
- ``_opening`` matches the opening and ``_closing`` the closing
  (see the :ref:`function signature section of the user reference<ref-func-sigs>` for terminology)
- ``_args`` matches a complete argument list
- ``RE_FUNC_SIG_EQ`` and ``.._MAPS`` match a complete function signature:
  equals-style and ``\mapsto``-style, respectively
  (like the vector regexes 1 and 2, they're only separate for readability)

So, for example, to make ``v`` always infer as vector, you'd edit one of the ``FUNC_ARG_VEC`` regexes,
to allow arguments to be separated by colons, you'd edit ``_args``.

.. note::

  Inferring scalar variables is done by checking whether they conatin an underscore.
  To change this, you need to edit the code in :meth:`latex2gnuplot.Leaf.extract_formulae` directly.


Small changes to the grammar
----------------------------
First, what do we mean by "small" changes?
Any changes that only add a new way to write something that's already possible now,
and thus only require editing the grammar itself (see below for examples).

To just add a "new way to spell things", add the new syntax to the grammar where appropriate.
Make sure to leave the AST after processing of renames as before your change.
That way, the unparsing code won't notice the grammar is different and work without any changes.

For example, you can add the asterisk as multiplikation operator
by replacing the ``"\\cdot"`` in the ``mult`` rule with ``("\\cdot" | "*")``.

As a more complex example, to add ``/`` as division operator with precedence between multiplications
and powers (which doesn't always look right, which is why it is not included),
you'd replace references to the ``*power`` nonterminals by ``*div`` nonterminals and
add new rules

.. code-block:: BNF

  div: power "/" power -> frac
  nindiv: ninpower "/" power -> frac
  ...

thinking carefully about what the modifiers should mean (another reason ``/`` isn't included).

.. note::

  If you want to add a function and the LaTeX command is the same as the gnuplot name,
  you just need to add it to the ``FUNCNAME`` terminal.

.. note::

  Before editing the grammar, read `lark's <https://pypi.org/project/lark>`_
  (the parsing library we use) grammar documentation.

Large changes to the grammar
----------------------------
If your changes aren't possible with the above method, you will have to add code to
the :class:`latex2gnuplot.transform.Values` class.
If your change is really exotic, add a new branch to :meth:`latex2gnuplot.transform.Values.unparse`.
You can avoid that in most simple cases:

- Your change is a node whose childern must be joined by something:
  In this case, add ``nodename: what_to_join_with`` to the ``joinables`` attribute.
  If the ``what_to_join_with`` is the empty string (e.g. because the operator is already included
  as a child node in the AST), you can just add it to the short list in the line below.
- You want to add a function with a different LaTeX command than gnuplot name:
  Add a ``latex_name: gnuplot_name`` entry to the ``func_names`` attribute.
- You want to add a constant (or anything that should be transformed to a constant string):
  add a ``latex_side: gnuplot_side`` entry to the ``constants`` attribute.

For example, to add ``\oplus`` as bitwise xor operator with precedence between ``sum`` and ``mult``,
you'd change the ``sum`` rule and add a ``xor`` rule:

.. code-block:: BNF

  sum: xor (/[+-]/ xor)*
  xor: mult ("\\oplus" mult)*

and then add ``'xor': '^'`` to the ``joinables`` attribute.

General extensions: attributes
------------------------------
If you want to add new functionality which needs to be configured
(e.g. the possibility to override inference),
you can make use of the general attribute system,
currently only used for figure templates.
See :class:`latex2gnuplot.document.Context`.
