.. LaTeX2gnuplot documentation master file, created by
   sphinx-quickstart on Sun Jun 18 20:21:22 2023.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to LaTeX2gnuplot's documentation!
=========================================

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   getting-started
   ref
   extending
   api

You're currently reading the index page, which isn't all that interesting.
If you want to use LaTeX2gnuplot, check out the :ref:`Getting Started <getting-started>` page.
If you already know the basics, but need something particular, the :ref:`reference <ref>` should have you covered.
The :ref:`Extending <extending>` page has useful information for small extensions,
like adding support for a new ``\function`` or tweaking default values.
For more complex extensions or developing LaTeX2gnuplot,
you'll probably want to read the :ref:`internal API documentation <api>`.

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
