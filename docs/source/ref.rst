.. _ref:

User reference
==============

This page contains detailed descriptions of accepted input.
If it doesn't exactly match what you want, you might be able
to :ref:`extend <extending>` LaTeX2gnuplot.
That page is also helpful if the explanations here aren't detailed enough,
since it will point you to the right place in the code to, for example, find a regex.

A word on variables, scalars and vectors
----------------------------------------

LaTeX2gnuplot only supports scalar calculations, but you can take a vector
as an argument and calculate with its entries.
To correctly parse everything, LaTeX2gnuplot needs to know beforehand
which variables a formula uses and whether these are scalars or vectors.
While the latter part is not critical, since in ambiguous situations,
LaTeX2gnuplot just tries both possibilities and takes the one that works,
knowing for sure does improve performance.

Variables and, if possible, their types, are inferred first from the signature,
if available, then from any projection specifications, and, if both fail,
``x`` is used as default variable of unknown type.

.. note::

  Variable and vector names must be exactly one lowercase latin character.

.. _ref-grammar:

Formula grammar
---------------

The formula grammar decides if and how a formula is converted by LaTeX2gnuplot.
The basic rules are as follows:

- You may use usual integer and decimal (with a dot ``.`` as separator) numbers.
  Do not add any spaces between the digits.
  For example, ``123`` and ``3.1415`` are OK, ``123 456`` and ``2,41`` are not.
- You may use any variables LaTeX2gnuplot knows about.
  If you use a vector entry (e.g. ``x_3``), put the index before a power.
  ``x_1^2`` is OK, ``x^2_1`` is not.
- You can use ``+``, ``-``, parentheses (including ``\left(`` and ``\right)``) as usual.
- You can use functions with a parenthesized argument, like ``\sin(2+3)``.
  You may not use a power directly on a function, e.g. ``\sin^2(x)`` won't work.
- You may write a multiplication as juxtaposition of factors, provided:

  * Only the first factor may be a number.
    If it is, the second factor must not be a power with a number as base.
  * You can chain multiplication juxtapositions with ``\cdot``.
  * At the end of such ``\cdot`` chaining, you may place a function with
    an unparenthesized argument consisting of a single multiplication juxtaposition.

The exact grammar used to parse a formula is available
`with the source <https://git.rwth-aachen.de/michael.klopsch/latex2gnuplot/latex2gnuplot/grammar.lark>`_.
You can use the ``--debug-parse`` option to see if and how a formula is parsed.


.. note::

  LaTeX2gnuplot tries to only parse unambiguous formulae,
  so everything it parses should be parsed the way you intend it to,
  but in case it isn't, the ``--debug-parse`` option prints the AST.

  If parsing fails, the error message should point you toword what's wrong.
  It might be some possible ambiguity, in which case parentehesis help,
  or a missing feature, in which case you might want to thing about
  :ref:`extending`.


.. _ref-proj:

Projection specifications
-------------------------

A projection specification consists of a target and
a space-separted (possibly empty) list of assignments.
The target may be a scalar variable or a vector entry (vector variable, underscore, index).
Each assignment consists of a target and a numerical value,
separated by an equals sign.
The target may be:

- a scalar variable or vector entry: in this case,
  it will be substituted by the value during conversion
- a vector variable, underscore, asterisk:
  in this case, all otherwise unset entries of the vector will be
  substituted by the value during conversion.
- a single asterisk: in this case, any otherwise unset scalar
  variables or vector entries will be substituted by the value during conversion.

The following are examples of valid projection specifications:

- ``g v_1=4.6 v_*=4 w=3`` (if ``g`` and ``w`` are scalar and ``v`` is a vector)
- ``b_2 b_*=-42 *=5 h=0`` (if ``h`` is a scalr and ``b`` is a vector)
- ``j`` (if ``j`` is scalar)

.. _ref-func-sigs:

Function signatures
-------------------

LaTeX2gnuplot tries to infer variables from function signatures.
A function signature consists of an "outside" part and the argument list.
In general, whitespace is allowed everywhere.

The "outside" part can be either a name (an alphabetical string),
a mandatory opening, the argument list, a mandatory closing and an equals sign
or an optional opening, the argument list, an optional closing and ``\mapsto``.
An opening ist an opening parenthesis or some ``\begin`` command, optionally receded by ``\left``.
Similarly, a closing is a closing parenthesis or some ``\end`` command, optionally preceded by ``\right``.

The argument list consists of arguments (surprise!) separated by commas or ``\\``.
An argument may be:

- a name (not inferred to be scalar or vector)
- a name, an underscore, and an index (inferred to be scalar, see note below)
- some TeX macro and a name, optionally enclosed in braces (inferred to be a vector)

For example, these signatures are all recognized:

- ``f(x) = ...``: ``x`` any
- ``h(x_1, \bar y )=``: ``x_1`` scalar, ``y`` vector
- ``g\begin{pmatrix} a \\ b \end{pmatrix} = ...``: ``a``, ``b`` any
- ``\myCoolVectorMarker{x} \mapsto ...``: ``x`` vector
- ``x, y \mapsto ...``: ``x``, ``y`` any

A possible workaround for an unrecognized signature you rely on is to
exclude the signature and add all arguments to a projection specification:

.. code-block:: tex

  While $$ \alpha(u) = 2u $$ doesn't work,
  $$ \alpha(u) =
  %gnuplot begin
  %gnuplot proj u
  2u
  %gnuplot end
  $$ does


.. note::

  Variable and vector names must be exactly one lowercase latin character.

.. note::

  Indexed variables are inferred as scalars because the entry itself is a scalar.
  This has a small negative side-effect: in a projection specification, you can't
  use a vector wildcard. You can work around this in (at least) two different ways:
  Just list all entries in the projection spec (you did in the signature) or
  exclude the signature by putting just the formula itself in a virtual environment.
