.. _getting-started:

Getting started
===============

The first section on this page covers everything you need to
convert your first document.
The other sections provide a how-to for common situations.
You'll probably want to at least skim over them to get an idea of what
LaTeX2gnuplot can and can't do.

Set-up
------

Before starting, make sure you have

- LaTeX2gnuplot (this example assumes it's in ``PATH``)
- gnuplot
- a LaTeX compiler

Assume you have the following short LaTeX document:

.. code-block:: tex

  \documentclass{article}
  \begin{document}
  The function $$ x^2 $$ is a quadratic function.
  \end{document}


We'll go through the steps needed to convert it with LaTeX2gnuplot.

LaTeX2gnuplot uses gnuplot's tikz terminal for output,
which uses a custom ``.sty`` file.
To include it, add ``\usepackage{gnuplot-lua-tikz}`` to your preamble.
To generate it, add the ``--create-style`` option the first time you invoke LaTeX2gnuplot.
Your file should now look like this:

.. code-block:: tex

  \documentclass{article}
  \usepackage{gnuplot-lua-tikz}
  \begin{document}
  The function $$ x^2 $$ is a quadratic function.
  \end{document}


You convert it with ``latex2gnuplot --create-style <example.tex >example.plot.tex``
(substitute file names of your choosing).
Compiling ``example.plot.tex`` should give you a document with a plot.
After the first time, you can leave out the ``--create-style`` option.

Basic customization
-------------------

By default, plots are plotted the... default way.
To add options, you can use special comments in yout LaTeX file.
For options you'd use with ``set`` in gnuplot, use ``%gnuplot set``,
for options added to the ``plot`` command, use ``%gnuplot plotopt``.

For example, you could use

.. code-block:: tex

  The function
  $$
  %gnuplot plotopt lc "red"
  %gnuplot set xrange [-10:0]
  (x-5)^2
  $$
  is a quadratic function, too.

to set the color to red and shift the x range.

.. note::

  Per comment, only add one option.


Projections
-----------

If you have a multi-dimensional function, like

.. code-block:: tex

  For natural $p$, the function $$ x^p $$ is a polynomial function.

it can be plotted in many different ways.
LaTeX2gnuplot projects it onto one variable,
which requires knowing which variable to project on
and what values the others should take.
Instead of guessing, LaTeX2gnuplot requires you to explicitly specify that.
You do that with a special comment of the form ``%gnuplot proj X Y=?...``
where you substitute ``X`` with the variable to project onto and
``Y=?...`` with a space separated list of values the other variables should assume.
For example,

.. code-block:: tex

  For natural $p$,
  $$
  %gnuplot proj x p=1
  %gnuplot proj x p=2
  %gnuplot proj x p=3
  x^p
  $$
  is a ploynomial function.

adds three plots.
See :ref:`the reference document <ref-proj>` for an in-depth explanation of the projection specification.

Formula selection
-----------------

While we only used the display math primitive ``$$`` in the examples above,
LaTeX2gnuplot also recognizes other math environments, e.g. ``equation``.
In the case of nested environments, formulae to plot are selected from the leaves.
Together with a "virtual" math environment, delimited by ``%gnuplot begin``
and ``%gnuplot end`` (alone on a line), this gives you a lot of flexibility in deciding
what to plot and what not to plot.

For example, to exclude a specific formula from being plotted,
add an empty virtual environment next to the formula.
That way, it won't be in a leaf:

.. code-block:: tex

  Unfortunately, the function
  $$ \weirdOperation{x}
  %gnuplot begin
  %gnuplot end
  $$
  cannot be plotted.

Conversely, to plot a formula that for some reason is not in a leaf,
just add a virtual environment around it, making it a leaf.
You can also "cut out" a formula if something around it prevents LaTeX2gnuplot from plotting it:

.. code-block:: tex

  However, we can plot the function
  $$ \mathbb{N} \to \mathbb{N},
  %gnuplot begin
  n \mapsto 2n
  %gnuplot end
  $$

Option scoping
--------------

Any options you set, like ``%gnuplot set``, ``%gnuplot plotopt`` and ``%gnuplto proj``,
are scoped by the environment they appear in. For example, in

.. code-block:: tex

  %gnuplot begin
  %gnuplot plotopt lc "red"
  The functions $$ x^2 $$ and $$ x^3 $$ are polynomial,
  %gnuplot end
  while $$ 2^x $$ is not.

the first two plots will be red, and the third will have the default color.
You can override options in child environments.

Customizing plot insertion
--------------------------

By default, plots are inserted as LaTeX figures with a basic caption
containing the plotted formula and, if applicable, the projection used.
You can customize this by setting the ``figure_tmpl`` attribute with
``%gnuplot figure_tmpl=...``. The text you set it to will be used as template for
any plot generated in its scope. Template variables to replace are preceded by an ampersand.
To insert a literal ampersand, use ``&&``.
LaTeX2gnuplot automatically sets these variables:

- ``plot`` to the generated plotting code
- ``latex`` to the formula that was plotted
- ``target`` to the variable projected on
- ``rule`` to the projection rule

You can also access any other attributes you define.
For example, to include a label, you might use:

.. code-block:: tex

  %gnuplot figure_tmpl=\begin{figure} &plot \caption{ $ &latex $ } \label{&label} \end{figure}
  The quadratic function
  $$
  %gnuplot label=fig:quadratic
  x^2
  $$
  is depicted in fig. \ref{fig:quadratic},
  while the cubic function
  $$
  %gnuplot label=fig:cubic
  x^3
  $$
  is depicted in fig. \ref{fig:cubic}

Converting lots of files
------------------------

LaTeX2gnuplot converts exactly one file per invokation,
reading from stdin and writing to stdout.
If you want to convert many files at once,
you're probably already using some kind of build system.
Just plug LaTeX2gnuplot in there.
For example, for make:

.. code-block:: make

  %.plot.tex: %.tex
	latex2gnuplot <$< >$@

Vectors and function signatures
--------------------------------

While LaTeX2gnuplot doesn't support vector calculations, you can define functions
taking a vector and calculating come scalar value from its entries, like
``x_1^2 + 5x_2`` works.
You can also add function signatures, which allow you to use different variables names,
e.g. ``k(w) = 2w``.

An in-depth explanation for both can be found in the :ref:`ref`,
but for simple cases, it should just work the way you'd imagine it to.
